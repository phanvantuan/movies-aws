#!/bin/bash
echo "Start deploy container - [START]";
DOCKER_STOP=`sudo docker stop movies-aws`
DOCKER_RM=`sudo docker rm movies-aws`
DOCKER_BUILD=`sudo docker build --tag movies-aws-image .`
DOCKER_RUN=`sudo docker run -it -d --name movies-aws -e PORT=443 --restart=always -p 443:443 movies-aws-image`
DOCKER_PRUNE=`sudo docker image prune -a --force`
sudo docker ps
echo "Finish deploy container - [END]";