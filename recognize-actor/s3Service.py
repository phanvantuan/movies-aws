import boto3
import numpy as np
import os.path
import cv2
from PIL import Image
from io import BytesIO
#bucket name va region name
S3_BUCKET_NAME = 'n10676881'
REGION_NAME = 'ap-southeast-2'
s3_client = boto3.client('s3')

def upload_file_to_s3(file_path, bucket_folder, bucket_name=S3_BUCKET_NAME, acl="public-read"):
        try:
            with open(file_path, "rb") as file:
                response = s3_client.upload_fileobj(file, bucket_name, bucket_folder, ExtraArgs={'ACL': 'public-read'});
                print (f'Uploaded status: {response}');
        except Exception as e:
            print(e)
            return False
        return True

def upload_bytes_to_s3(file, bucket_folder, bucket_name=S3_BUCKET_NAME, acl="public-read"):
    try:
        response = s3_client.put_object(
            Bucket=bucket_name,
            Body=file,
            Key=bucket_folder
        );
        print (f'Uploaded status: {response}');
    except Exception as e:
        print(e)
        return False
    return True

def get_images_from_s3(image_key, bucket_name=S3_BUCKET_NAME):
    imageObj = s3_client.get_object(Bucket=bucket_name, Key=image_key )
    # reading the file content in bytes

    file_content = imageObj["Body"].read()
    image = Image.open(BytesIO(file_content)).convert('L')
    # //*///////
    # imageNp = np.array(image,'uint8')
    # cv2.imshow('image window', imageNp)
    # # add wait key. window waits until user presses a key
    # cv2.waitKey(0)
    # # and finally destroy/close all open windows
    # cv2.destroyAllWindows()
    return image

def download_from_s3(saved_path, image_key, bucket_name=S3_BUCKET_NAME):
    # imageObj = s3_client.get_object(Bucket=bucket_name, Key=image_key )
    # reading the file content in bytes
    try:
        if(os.path.exists(saved_path) == False):
            os.makedirs(saved_path)
        s3_client.download_file(bucket_name, image_key, saved_path + '/models.yml')
    except Exception as err:
        print("download failed")
        print(err)
        return False
    return True

# get_images_from_s3('images/1039523.pij9g793mb2ZGyyNdEI7TOoJ6i8.jpg')