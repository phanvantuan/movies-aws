import sys
sys.path.append('/recognize-actor')
from recognizer import processRecognizer
from flask import jsonify, request, abort, Flask
from io import BytesIO  
from PIL import Image

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

app = Flask(__name__)

@app.route('/file-upload', methods=['POST'])
def recognizeImage():
	# check if the post request has the file part
	if 'file' not in request.files:
		resp = jsonify({'message' : 'No file part in the request', 'code': 400})
		return resp
	file = request.files['file']
	if file.filename == '':
		resp = jsonify({'message' : 'No file selected for uploading', 'code': 400})
		return resp
	if file and allowed_file(file.filename):
		image = Image.open(file).convert('L')
		results = processRecognizer(image)
		resp = jsonify({'actor' : results, 'code': 200})
		return resp
	else:
		resp = jsonify({'message' : 'Allowed file types are png, jpg, jpeg', 'code': 400})
		return resp


if __name__ == "__main__":
    app.run()