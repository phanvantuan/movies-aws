from asyncio import tasks
from mysql.connector import connection
import requests
import asyncio
import database
import aiohttp
import sys
sys.path.append('/recognize-actor')
import s3Service

API_KEY = '53f58793c3dba26e0735b222ac0b32c2'
BASE_API_PATH = 'https://api.themoviedb.org/3'
BASE_IMAGE_PATH = 'https://image.tmdb.org/t/p/w300'


async def downloadImage(url, path):
    images = requests.get(url)
    s3Service.upload_bytes_to_s3(images.content, path)
    print(f"Upload image to s3 success path {path}")
    # open(path, 'wb').write(images.content)

async def fetchImageOfActor(actorId):
    results = requests.get(f'{BASE_API_PATH}/person/{actorId}/images', params={'api_key': API_KEY})
    resultAsJson = results.json();
    profiles = resultAsJson['profiles']
    listImages = [];
    listPromise = []
    for profile in profiles:
        imageURL = profile['file_path']
        downloadImageUrl = f'{BASE_IMAGE_PATH}{imageURL}'
        pathSaveImage = f'./recognize-actor/dataset/{actorId}.{imageURL[1:]}'
        pathS3SaveImage = f'images/{actorId}.{imageURL[1:]}'
        listImages.append(imageURL)
        listPromise.append(downloadImage(downloadImageUrl, pathS3SaveImage))

    await asyncio.gather(*listPromise, return_exceptions=True)
    return listImages


async def fetchDataFromAPI(connectionDB, page, session):
    async with session.get(f'{BASE_API_PATH}/person/popular', params={'api_key': API_KEY, 'page':page}) as actorArr:
        actorArrAsJson = actorArr.json();
        listActor = actorArrAsJson['results'];
        tasks = []
        if(connectionDB == None):
            connectionDB = database.createDatabaseConnection()
        for actor in listActor:
            task = asyncio.ensure_future(fetchImageOfActor(actor['id']))
            tasks.append(task)
            
            sql = "insert into actors (id, known_for_department, fullName, profile_path) values (%s, %s, %s, %s) ON DUPLICATE KEY UPDATE known_for_department = %s, fullName = %s, profile_path = %s"
            values = (actor['id'], actor['known_for_department'], actor['name'], actor['profile_path'], actor['known_for_department'], actor['name'], actor['profile_path'])
            connectionDB.cursor().execute(sql, values)
            connectionDB.commit()
        
        if(len(tasks) > 1):
            await asyncio.gather(*tasks, return_exceptions=True)

async def wrapperFetchActor():
    connectionDB = None
    tasks = []
    async with aiohttp.ClientSession() as session:
        for i in range(500):
            task = asyncio.ensure_future(fetchDataFromAPI(connectionDB, i + 1, session))
            tasks.append(task)
        if(len(tasks) > 1):
            await asyncio.gather(*tasks, return_exceptions=True)
        print('fetch success')


async def fetchDetailFilms(page, session, connectionDB):
    print(f"Fetch films page = {page}")
    async with session.get(f'{BASE_API_PATH}/movie/popular', params={'api_key': API_KEY, 'page': page}) as films:
        filmsArrAsJson = await films.json();
        listFilms = filmsArrAsJson['results'];
        print(listFilms[0])
        if(len(listFilms) < 1):
            print ('Films empty');
            return False;
        
        if(connectionDB == None):
            connectionDB = database.createDatabaseConnection()
        dbCursor = connectionDB.cursor()
        sql = """INSERT INTO film_details (id, backdrop_path, original_title, title, overview, popularity, poster_path, release_date, vote_average, vote_count) VALUES (%s, %s, %s, %s,%s, %s, %s, %s,%s, %s) ON DUPLICATE KEY UPDATE title = %s """
        for film in listFilms:
            value = (str(film['id']), film['backdrop_path'], film['original_title'], film['title'], film['overview'], str(film['popularity']), film['poster_path'], film['release_date'], str(film['vote_average']), str(film['vote_count']), film['title'])
            dbCursor.execute(sql, value)

        connectionDB.commit()
        print(f"Completed fetch films page = {page}")
        return True

async def wrapperFetchFilms():
    connectionDB = None
    tasks = []
    async with aiohttp.ClientSession() as session:
        for i in range(500):
            task = asyncio.ensure_future(fetchDetailFilms(i + 1, session, connectionDB))
            tasks.append(task)
        if(len(tasks) > 1):
            await asyncio.gather(*tasks, return_exceptions=True)
        print('fetch success')


def mainFunc():
    connectionDB = None
    print("Begin prepare training data")
    # asyncio.get_event_loop().run_until_complete(wrapperFetchFilms())
    asyncio.get_event_loop().run_until_complete(fetchDataFromAPI(connectionDB))
    print("End prepare training data")

# mainFunc()