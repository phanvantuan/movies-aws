import mysql.connector

HOST = 'moviesdbinstance.ce2haupt2cta.ap-southeast-2.rds.amazonaws.com'
USER = 'root'
PASSWORD = 'movies1234'
DATABASE = 'moviesdb'

def createDatabaseConnection():
    return mysql.connector.connect(
        host = HOST,
        user = USER,
        password = PASSWORD,
        database = DATABASE,
        auth_plugin='mysql_native_password'
    )