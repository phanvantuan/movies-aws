import cv2,os
import numpy as np
from PIL import Image
import sys
sys.path.append('/recognize-actor')
import s3Service
detector= cv2.CascadeClassifier("./recognize-actor/haarcascade_frontalface_default.xml")
recognizer = cv2.face.LBPHFaceRecognizer_create()
S3_BUCKET_NAME = 'n10676881'
REGION_NAME = 'ap-southeast-2'
import boto3
s3_client = boto3.client('s3')

def getImagesAndLabels(path):
    # Load all image in train folder
    # imagePaths=[os.path.join(path,f) for f in os.listdir(path)] 

    #create empty face list
    faceSamples=[]
    #create empty ID list
    Ids=[]
    paginator = s3_client.get_paginator("list_objects_v2")
    pages = paginator.paginate(Bucket=S3_BUCKET_NAME, Prefix='images/')
    for page in pages:
        try:
            contents = page["Contents"]
        except KeyError:
            break

        for image in contents:
            imageS3Url = image["Key"]
            print(f'Training image {imageS3Url}')
            pilImage = s3Service.get_images_from_s3(imageS3Url)
            #loading the image and converting it to gray color
            # pilImage=Image.open(imagePath).convert('L')
            #Convert image into numpy array
            imageNp=np.array(pilImage,'uint8')
            #Extract the Id from the image
            Id=int(imageS3Url.split("/")[1].split('.')[0])
            # extract the face from the training image sample
            faces=detector.detectMultiScale(imageNp)
            #If a face is there then append that in the list as well as Id of it
            for (x,y,w,h) in faces:
                faceSamples.append(imageNp[y:y+h,x:x+w])
                Ids.append(Id)
    return faceSamples,Ids
    

# Get all faces and ids
faceSamples,Ids = getImagesAndLabels('./recognize-actor/dataset')

# Train models
recognizer.train(faceSamples, np.array(Ids))

# save recognize models
recognizer.save('./recognize-actor/train-models/models.yml')
s3Service.upload_file_to_s3('./recognize-actor/train-models/models.yml', 'models/models.yml')

print("Trained models complete!")