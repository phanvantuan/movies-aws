import cv2
import numpy as np
from PIL import Image
import os.path
import sys
sys.path.append('/recognize-actor')
import s3Service
# Init detect face models
faceDetect=cv2.CascadeClassifier('recognize-actor/haarcascade_frontalface_default.xml');
recognizer = cv2.face.LBPHFaceRecognizer_create()
if(os.path.exists('recognize-actor/train-models/models.yml')):
    # Init machine learning models
    recognizer.read('recognize-actor/train-models/models.yml')
else:
    modelsFile = s3Service.download_from_s3('recognize-actor/train-models','models/models.yml')
    if(modelsFile):
        recognizer.read('recognize-actor/train-models/models.yml')


id=0
fontface = cv2.FONT_HERSHEY_SIMPLEX
fontscale = 1
fontcolor = (0,255,0)
fontcolor1 = (0,0,255)

def processRecognizer(pilImage):
    #Convert image into numpy array
    imageNp=np.array(pilImage,'uint8')
    # get width, height origin of image
    widthOrigin, heightOrigin = pilImage.size
    print("Original Height and Width:", widthOrigin,"x", heightOrigin)
    # width = 300
    # height = 450
    # dim = (width, height)
    # resize image
    # resized = cv2.resize(imageNp, dim, interpolation = cv2.INTER_AREA)
    
    # Detect faces in image
    faces=faceDetect.detectMultiScale(imageNp,1.3,5);

    # Loop iter faces
    for(x,y,w,h) in faces:
        # Detecting face, return actorId and dist
        id,dist=recognizer.predict(imageNp[y:y+h,x:x+w])
        
        # cv2.imshow('image window', imageNp[y:y+h,x:x+w])
        # add wait key. window waits until user presses a key
        # cv2.waitKey(0)
        # and finally destroy/close all open windows
        # cv2.destroyAllWindows()
        
        # Nếu độ sai khác < 50% thì lấy profile
        if (dist<=50):
            print(f'Actor: {id}')
            return id
    
    print('Unknown actor')
    return '';

# processRecognizer()

# def mainFunc():
#     imagePath = 'recognize-actor/dataset/1368507.xNJocGBlqb8NXXSgzxAMtu87dqy.jpg'
#     #loading the image and converting it to gray scale
#     pilImage=Image.open(imagePath).convert('L')
#     processRecognizer(pilImage)

# mainFunc()