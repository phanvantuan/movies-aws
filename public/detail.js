const APP_ID_FB = '1296965620706643';
const PAGE_ID = '';
const access_token = 'EAAC1ztmeEkgBAItTGLmcK3IQp6CoXfD8KHFyMOObFZBxZCJ5ngcZBccBRn3hGzk20vAFOBPZAfI0QG94opZBZAJvZCB5ZAK6LKZC6YUL7u2uQrZAkvy8lJWImXVSNUbKsQ12MjIc1KaTp6U5zKOC9uqTMDKMkzZCei3CZBZBXvMZBQ7jdTklCuHfaTZAwI0';
const BASE_URL = `https://13.211.59.194`;
const api = 'https://api.themoviedb.org/3/';
const api_key = 'api_key=53f58793c3dba26e0735b222ac0b32c2';
const url_image = 'https://image.tmdb.org/t/p/w500';

async function GetData(query) {
  const res1 = await fetch(query, { headers: {} });
  const data = await res1.json();
  return data;
}

async function ShowDetailMovie1(id) {
  $('.search-tools').empty();
  $('.card1').empty();
  $('.card1').append(`
        <div class="spinner-grow text-success" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-danger" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
          <span class="sr-only">Loading...</span>
        </div>
    `);
  query = `${api}movie/${id}?${api_key}`;
  // detailmovie = await GetData(query);
  query1 = `${api}movie/${id}/reviews?${api_key}`;
  // reviews = await GetData(query1);
  // console.log(reviews);
  const [detailmovie, reviews] = await Promise.all([GetData(query), GetData(query1)])

  genres = '';
  for (let g of detailmovie.genres) {
    genres += g.name + ',';
  }

  $('.card1').empty();
  $('.card2').empty();

  $('.card1').append(`
        
  <div class="row no-gutters bg-light">
    <div class="col-md-6 d-flex justify-content-center">
      <img src="${url_image}${detailmovie.backdrop_path}" class="card-img p-3" alt="..." onerror="this.src='http://www.amulyamica.com/files/noimage.jpg'">
    </div>
    <div class="col-md-6">
      <div class="card-body">
        <h5 class="card-title h3 text-primary text-center">${detailmovie.original_title}</h5>
        <p class="card-text-genres text-secondary">Genres: ${genres}...</p>
        <p class="card-text text-secondary">Release Date: ${detailmovie.release_date}.</p>
        
        <p class="card-text font-italic">Overview: '${detailmovie.overview}.'</p>
        <button class="btn btn-outline-success my-2 my-sm-0 ml-3" onclick="publishingMoviesFacebook('${detailmovie.original_title} - ${movie.id}')">Share to FB</button>
        </div>
    </div>
  </div>

    `);

  $('.card-review').empty();
  $('.card-review').append(`
        
        <div class="card w-100">
            <div class="card-header">
                <p class="card-text font-weight-bold text-success">Reviews</p>
            </div>
        </div>

    `);
  if (reviews.total_results == 0) {
    $('.card-review .card').append(`
            <div class="card-body">
            <blockquote class="blockquote">
              <p>No Review</p>
            </blockquote>
            </div>
        `);
  } else {
    for (let r of reviews.results) {
      $('.card-review .card').append(`
        
            <div class="card-body">
                <p class="card-text font-italic">${r.content}</p>
                <footer class="blockquote-footer">${r.author}</footer>
            </div>
            
    
        `);
    }
  }

  await actorpagination(id, 1);
  window.scrollTo({
    top: 0,
    behavior: 'smooth'
  });
}

async function actorpagination(id, page) {
  let query1 = `${api}movie/${id}/credits?${api_key}`;
  // credits = await GetData(query1);
  let query = `${api}movie/${id}/recommendations?${api_key}`;
  // listRecommendations = await GetData(query);
  const [credits, listRecommendations] = await Promise.all([GetData(query1), GetData(query)])
  console.log(credits);

  if ($('.card-text-director').length == 0) {
    $('.card-text-genres').after(`<p class="card-text-director text-secondary">Director: ${credits.crew.find(c => c.job == 'Director').name}</p>`);
  }

  $('.card2').empty();
  for (let i = (page - 1) * 5; i < (page * 5 > credits.cast.length ? credits.cast.length : page * 5); i++) {
    $('.card2').append(`
            <div class="card mr-1" style="width: 13rem;" onclick="ShowDetailActor(${credits.cast[i].id})">
                <img src="${url_image}${credits.cast[i].profile_path}" class="card-img-top" alt="..." onerror="this.src='http://www.amulyamica.com/files/noimage.jpg'">
                <div class="card-body">
                <p class="card-text text-danger font-weight-bold">${credits.cast[i].character}</p>
                <p class="card-text">Name: ${credits.cast[i].name}</p>
                <p class="card-text">Sex: ${credits.cast[i].gender == 1 ? 'Female' : 'Male'}</p>
                </div>
            </div>
        `);
  }

  leng = Math.ceil(credits.cast.length / 5);
  console.log(leng);
  from = page - 1 == 0 ? 1 : page - 1;
  to = from + 4;

  if (to > leng) {
    to = leng;
    from = to - 5 < 1 ? 1 : to - 5;
  }

  $('.pagination').empty();
  $('.pagination').append(`
        <li class="page-item">
          <a class="page-link " href="#" tabindex="-1" aria-disabled="true" onclick="actorpagination(${id},1);">&laquo;</a>
        </li>
        
        
        <li class="page-item">
          <a class="page-link" href="#" onclick="actorpagination(${id},${leng});">&raquo;</a>
        </li>
        
    `)
  for (let i = from; i <= to; i++) {
    $('.pagination li:last-child').before(`<li class="page-item ${i === page ? 'active' : ''}"><a class="page-link" href="#" onclick="actorpagination(${id},${i});">${i}</a></li>`)
  }

  if(listRecommendations && listRecommendations.results){
    listRecommendations.results.length = 5;
    const renderHTMLRecommendations = listRecommendations.results.map(mv => `
    <div class="card mr-1" style="width: 13rem;" onclick="ShowDetailMovie(${mv.id})">
    <img src="${url_image}${mv.poster_path}" class="card-img-top" alt="..." onerror="this.src='http://www.amulyamica.com/files/noimage.jpg'">
    <div class="card-body">
    <p class="card-text text-danger font-weight-bold">${mv.original_title}</p>
    <p class="card-text">Release date: ${mv.release_date}</p>
    <p class="card-text">Popularity: ${mv.popularity}</p>
    </div>
  </div>
    `).join('\n');
    $('.recommendations').empty();
    $('.pagination').after(`
    <div class="row1-md mt-2 recommendations">
      <h3 class="recommend-for-you">Recommend for you</h3>
      <div class="card3 d-flex flex-wrap justify-content-center mt-2">${renderHTMLRecommendations}</div>
    </div>
  `)
  }

}

async function ShowDetailMovie(id) {
  $('.search-tools').empty();
  $('.card1').empty();
  $('.card1').append(`
        <div class="spinner-grow text-success" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-danger" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
          <span class="sr-only">Loading...</span>
        </div>
    `);
  query = `${api}movie/${id}?${api_key}`;
  // detailmovie = await GetData(query);
  query1 = `${api}movie/${id}/reviews?${api_key}`;
  // reviews = await GetData(query1);
  // console.log(reviews);
  const [detailmovie, reviews] = await Promise.all([GetData(query), GetData(query1)])

  genres = '';
  for (let g of detailmovie.genres) {
    genres += g.name + ',';
  }

  $('.card1').empty();
  $('.card2').empty();

  $('.card1').append(`
        
  <div class="row no-gutters bg-light">
    <div class="col-md-6 d-flex justify-content-center">
      <img src="${url_image}${detailmovie.backdrop_path}" class="card-img p-3" alt="..." onerror="this.src='http://www.amulyamica.com/files/noimage.jpg'">
    </div>
    <div class="col-md-6">
      <div class="card-body">
        <h5 class="card-title h3 text-primary text-center">${detailmovie.original_title}</h5>
        <p class="card-text-genres text-secondary">Genres: ${genres}...</p>
        <p class="card-text text-secondary">Release Date: ${detailmovie.release_date}.</p>
        
        <p class="card-text font-italic">Overview: '${detailmovie.overview}.'</p>
        <div id="shareBtn" class="btn btn-success clearfix">Share Facebook</div>
        <script>
        document.getElementById('shareBtn').onclick = function() {
          FB.ui({
            display: 'popup',
            method: 'share',
            href: '${BASE_URL}/movie/${detailmovie.id}',
          }, function(response){});
        }
        </script>
        </div>
    </div>
  </div>

    `);

  $('.card-review').empty();
  $('.card-review').append(`
        
        <div class="card w-100">
            <div class="card-header">
                <p class="card-text font-weight-bold text-success">Reviews</p>
            </div>
        </div>

    `);
  if (reviews.total_results == 0) {
    $('.card-review .card').append(`
            <div class="card-body">
            <blockquote class="blockquote">
              <p>No Review</p>
            </blockquote>
            </div>
        `);
  } else {
    for (let r of reviews.results) {
      $('.card-review .card').append(`
        
            <div class="card-body">
                <p class="card-text font-italic">${r.content}</p>
                <footer class="blockquote-footer">${r.author}</footer>
            </div>
            
    
        `);
    }
  }

  await actorpagination(id, 1);
  window.scrollTo({
    top: 0,
    behavior: 'smooth'
  });
}

async function moviepagination(id, page) {
  $('.recommendations').empty();
  $('.card2').empty();
  $('.card2').append(`
        <div class="spinner-grow text-success" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-danger" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
          <span class="sr-only">Loading...</span>
        </div>
    `);

  query = `${api}person/${id}/movie_credits?${api_key}&page=${page}`;
  movies = await GetData(query);

  $('.card2').empty();
  for (let i = (page - 1) * 5; i < (page * 5 > movies.cast.length ? movies.cast.length : page * 5); i++) {
    $('.card2').append(`
            <div class="card mr-1" style="width: 13rem;" onclick="ShowDetailActor(${movies.cast[i].id})">
                <img src="${url_image}${movies.cast[i].poster_path}" class="img-thumbnail rounded d-block mr-3 ml-1" style="width: 200px; height: 200px;" onerror="this.src='http://www.amulyamica.com/files/noimage.jpg'" alt="..." onclick="ShowDetailMovie(${movies.cast[i].id})">
            <div class="card-body">
                <p class="card-text text-danger font-weight-bold">${movies.cast[i].original_title}</p>
                <p class="card-text">Character: ${movies.cast[i].character}</p>
                <p class="card-text">Release Date: ${movies.cast[i].release_date}</p>
                </div>
            </div>
            
        `);
  }

  leng = Math.ceil(movies.cast.length / 5);
  console.log(leng);
  from = page - 1 == 0 ? 1 : page - 1;
  to = from + 4;

  if (to > leng) {
    to = leng;
    from = to - 5 < 1 ? 1 : to - 5;
  }

  $('.pagination').empty();
  $('.pagination').append(`
        <li class="page-item">
          <a class="page-link " href="#" tabindex="-1" aria-disabled="true" onclick="moviepagination(${id},1);">&laquo;</a>
        </li>
        
        
        <li class="page-item">
          <a class="page-link" href="#" onclick="moviepagination(${id},${leng});">&raquo;</a>
        </li>
        
    `);

  for (let i = from; i <= to; i++) {
    $('.pagination li:last-child').before(`<li class="page-item ${i === page ? 'active' : ''}" name="${i}"><a class="page-link" href="#" onclick="moviepagination(${id},${i});">${i}</a></li>`)
  }

}



window.fbAsyncInit = function() {
  FB.init({
  appId      : APP_ID_FB,
  cookie     : true,
  xfbml      : true,
  version    : 'v12.0'
  });
};

//  function publishingMoviesFacebook(message = 'hello world', link = ''){
//   FB.getLoginStatus(async(res) => {
//     console.log(res);
//     if(res.status != "connected"){
//       // FB.login((response) => {
//         FB.api(
//           `/${PAGE_ID}/feed`,
//           'POST',
//           {"message": message, link: `${BASE_URL}${link}`, "access_token": access_token},
//           function(response) {
//             console.log('response >>> ', response)
//             if(response.error){
//               alert("You dont have permission");
//             }else{
//               alert("publish successfully");
//             }
//           }
//         );
//       // }, {scope: 'email, pages_show_list, publish_to_groups, pages_read_engagement, pages_read_user_content, pages_manage_posts, pages_manage_engagement, public_profile' })
//     }else{
//       FB.api(
//         `/${PAGE_ID}/feed`,
//         'POST',
//         {"message": message, link:`${BASE_URL}${link}`, "access_token": access_token},
//         function(response) {
//           if(response.error){
//             alert("You dont have permission");
//           }else{
//             alert("publish successfully");
//           }
//         }
//       );
//     }
//   })
// }

// async function loginWithFB(message, link){
//   FB.login((res) => {
//     FB.api(
//       `/${PAGE_ID}/feed`,
//       'POST',
//       {"message": message, link, "access_token": access_token},
//       function(response) {
//         console.log('response >>> ', response)
//         if(response.error){
//           alert("You dont have permission");
//         }else{
//           alert("publish successfully");
//         }
//       }
//     );
//   }, {scope: 'email, pages_show_list, publish_to_groups, pages_read_engagement, pages_read_user_content, pages_manage_posts, pages_manage_engagement, public_profile' })
// }

// function logoutWithFB(){
//   FB.logout((res) => {
//     console.log(res)
//   })
// }

function Search(evt) {
  evt.preventDefault();
  Search1(1);
}

async function Search1(page) {
  $('.recommendations').empty();
  $('.search-tools').empty();
  $('.card2').empty();
  $('.card1').empty();
  $('.card1').append(`
        <div class="spinner-grow text-success" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-danger" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
          <span class="sr-only">Loading...</span>
        </div>
    `);
  query = `${api}search/movie?${api_key}&page=${page}&query=${$('.search-input').val()}`;
  movies = await GetData(query);
  console.log(movies);
  $('.card1').empty();
  $('.card2').empty();
  $('.card-review').empty();
  if (movies.total_results == 0 || page > movies.total_pages) {
    $('.card1').append(`
            <img src="https://bsmedia.business-standard.com/_media/bs/theme/faq_view_all/images/no-result-found.png" class="img-fluid" alt="Responsive image">
            `);
    return;
  }
  for (let movie of movies.results) {
    if (movie.media_type == 'person') {

      for (let m of movie.known_for) {
        $('.card2').append(`
                <a class="card mr-2 mt-2" style="width: 16rem;" href="/movie/${m.id}">
                <div class="hovereffect">
                  <img class="card-img-top" src="${url_image}${m.poster_path}" onerror="this.src='http://www.amulyamica.com/files/noimage.jpg'">
                <div class="overlay">
                    <p class="h3 bg-info font-weight-bold">${m.original_title}</p>
                    <p class="text-monospace text-danger">Vote_Average: ${m.vote_average}</p>
                    <p class="text-monospace">${m.release_date}</p>
                </div>
                  </div>
                </a>
                `);
      }

    } else  {
      $('.card2').append(`
            <a class="card mr-2 mt-2" style="width: 16rem;" href="/movie/${movie.id}">
            <div class="hovereffect">
              <img class="card-img-top" src="${url_image}${movie.poster_path}" onerror="this.src='http://www.amulyamica.com/files/noimage.jpg'">
              <div class="overlay">
                <p class="h3 bg-info font-weight-bold">${movie.original_title}</p>
                <p class="text-monospace text-danger">${movie.vote_average}</p>
                <p class="text-monospace">${movie.release_date}</p>
              </div>
              </div>
            </a>
            `);
    }
  }


  from = page - 1 == 0 ? 1 : page - 1;

  to = from + 4;
  if (to > movies.total_pages) {
    to = movies.total_pages;
    from = to - 5 < 1 ? 1 : to - 5;
  }

  $('.pagination').empty();
  $('.pagination').append(`
        <li class="page-item">
          <a class="page-link " href="#" tabindex="-1" aria-disabled="true" onclick="Search1(1);">&laquo;</a>
        </li>
        
        
        <li class="page-item">
          <a class="page-link" href="#" onclick="Search1(${movies.total_pages});">&raquo;</a>
        </li>
        
    `);

  for (let i = from; i <= to; i++) {
    $('.pagination li:last-child').before(`<li class="page-item ${i === page ? 'active' : ''}" name="${i}"><a class="page-link" href="#" onclick="Search1(${i});">${i}</a></li>`)
  }
}
