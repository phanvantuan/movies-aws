const api = 'https://api.themoviedb.org/3/';
const api_key = 'api_key=53f58793c3dba26e0735b222ac0b32c2';
const url_image = 'https://image.tmdb.org/t/p/w500';

async function GetData(query) {
    const res1 = await fetch(query, { headers: {} });
    const data = await res1.json();
    return data;
}

async function ShowInfoDetectDetailActor(id) {
    $('.search-tools').empty();
    $('.card1').empty();
    $('.card2').empty();
    $('.card-review').empty();
    $('.card1').append(`
        <div class="spinner-grow text-success" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-danger" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
          <span class="sr-only">Loading...</span>
        </div>
    `);

    $('.card1').empty();
    if (!id) {
        $('.card1').append(`
                <img src="https://bsmedia.business-standard.com/_media/bs/theme/faq_view_all/images/no-result-found.png" class="img-fluid" alt="Responsive image">
        `);
        return;
    }

    query = `${api}person/${id}?${api_key}`;
    person = await GetData(query);

    $('.card1').append(`
	<div class="row no-gutters bg-light">
	    <div class="col-md-6 d-flex justify-content-center">
	      <img src="${url_image}${person.profile_path}" class="card-img p-3" onerror="this.src='http://www.amulyamica.com/files/noimage.jpg'" alt="...">
	    </div>
	    <div class="col-md-6">
	    	<div class="card-body">
            <h5 class="card-title h3 text-primary text-center">${person.name}</h5>
            <p class="card-text-genres text-secondary">Birthday: ${person.birthday}</p>
            <p class="card-text font-italic">Biography: '${person.biography}.'</p>
	    	</div>
	    </div>
	</div>
	`);
    moviepagination(id, 1);
    window.scrollTo({
        top: 0,
        behavior: 'smooth'
    });
}

async function moviepagination(id, page) {
    $('.recommendations').empty();
    $('.card2').empty();
    $('.card2').append(`
        <div class="spinner-grow text-success" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-danger" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
          <span class="sr-only">Loading...</span>
        </div>
    `);

    query = `${api}person/${id}/movie_credits?${api_key}&page=${page}`;
    movies = await GetData(query);

    $('.card2').empty();
    for (let i = (page - 1) * 5; i < (page * 5 > movies.cast.length ? movies.cast.length : page * 5); i++) {
        $('.card2').append(`
            <div class="card mr-1" style="width: 13rem;" onclick="ShowDetailActor(${movies.cast[i].id})">
                <img src="${url_image}${movies.cast[i].poster_path}" class="img-thumbnail rounded d-block mr-3 ml-1" style="width: 200px; height: 200px;" onerror="this.src='http://www.amulyamica.com/files/noimage.jpg'" alt="..." onclick="ShowDetailMovie(${movies.cast[i].id})">
            <div class="card-body">
                <p class="card-text text-danger font-weight-bold">${movies.cast[i].original_title}</p>
                <p class="card-text">Character: ${movies.cast[i].character}</p>
                <p class="card-text">Release Date: ${movies.cast[i].release_date}</p>
                </div>
            </div>
            
        `);
    }

    leng = Math.ceil(movies.cast.length / 5);
    console.log(leng);
    from = page - 1 == 0 ? 1 : page - 1;
    to = from + 4;

    if (to > leng) {
        to = leng;
        from = to - 5 < 1 ? 1 : to - 5;
    }

    $('.pagination').empty();
    $('.pagination').append(`
        <li class="page-item">
          <a class="page-link " href="#" tabindex="-1" aria-disabled="true" onclick="moviepagination(${id},1);">&laquo;</a>
        </li>
        
        
        <li class="page-item">
          <a class="page-link" href="#" onclick="moviepagination(${id},${leng});">&raquo;</a>
        </li>
        
    `);

    for (let i = from; i <= to; i++) {
        $('.pagination li:last-child').before(`<li class="page-item ${i === page ? 'active' : ''}" name="${i}"><a class="page-link" href="#" onclick="moviepagination(${id},${i});">${i}</a></li>`)
    }

}

async function handleDetectActor(file){
    const formData = new FormData();
    // const fileField = document.querySelector('input[type="file"]');
    formData.append('file', file);
    fetch('/actors', {
        method: 'POST',
        body: formData
    })
        .then(response => response.json())
        .then(result => {
            console.log('Success:', result);
            ShowInfoDetectDetailActor(result.actor)
        })
        .catch(error => {
            console.error('Error:', error);
        });

    
}

async function ShowDetailMovie(id) {
    $('.search-tools').empty();
    $('.card1').empty();
    $('.card1').append(`
        <div class="spinner-grow text-success" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-danger" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
          <span class="sr-only">Loading...</span>
        </div>
    `);
    query = `${api}movie/${id}?${api_key}`;
    // detailmovie = await GetData(query);
    query1 = `${api}movie/${id}/reviews?${api_key}`;
    // reviews = await GetData(query1);
    // console.log(reviews);
    const [detailmovie, reviews] = await Promise.all([GetData(query), GetData(query1)])

    genres = '';
    for (let g of detailmovie.genres) {
        genres += g.name + ',';
    }

    $('.card1').empty();
    $('.card2').empty();

    $('.card1').append(`
        
  <div class="row no-gutters bg-light">
    <div class="col-md-6 d-flex justify-content-center">
      <img src="${url_image}${detailmovie.backdrop_path}" class="card-img p-3" alt="..." onerror="this.src='http://www.amulyamica.com/files/noimage.jpg'">
    </div>
    <div class="col-md-6">
      <div class="card-body">
        <h5 class="card-title h3 text-primary text-center">${detailmovie.original_title}</h5>
        <p class="card-text-genres text-secondary">Genres: ${genres}...</p>
        <p class="card-text text-secondary">Release Date: ${detailmovie.release_date}.</p>
        
        <p class="card-text font-italic">Overview: '${detailmovie.overview}.'</p>
      </div>
    </div>
  </div>

    `);

    $('.card-review').empty();
    $('.card-review').append(`
        
        <div class="card w-100">
            <div class="card-header">
                <p class="card-text font-weight-bold text-success">Reviews</p>
            </div>
        </div>

    `);
    if (reviews.total_results == 0) {
        $('.card-review .card').append(`
            <div class="card-body">
            <blockquote class="blockquote">
              <p>No Review</p>
            </blockquote>
            </div>
        `);
    } else {
        for (let r of reviews.results) {
            $('.card-review .card').append(`
        
            <div class="card-body">
                <p class="card-text font-italic">${r.content}</p>
                <footer class="blockquote-footer">${r.author}</footer>
            </div>
            
    
        `);
        }
    }

    await actorpagination(id, 1);
    window.scrollTo({
        top: 0,
        behavior: 'smooth'
    });
}

async function actorpagination(id, page) {
    let query1 = `${api}movie/${id}/credits?${api_key}`;
    // credits = await GetData(query1);
    let query = `${api}movie/${id}/recommendations?${api_key}`;
    // listRecommendations = await GetData(query);
    const [credits, listRecommendations] = await Promise.all([GetData(query1), GetData(query)])
    console.log(credits);

    if ($('.card-text-director').length == 0) {
        $('.card-text-genres').after(`<p class="card-text-director text-secondary">Director: ${credits.crew.find(c => c.job == 'Director').name}</p>`);
    }

    $('.card2').empty();
    for (let i = (page - 1) * 5; i < (page * 5 > credits.cast.length ? credits.cast.length : page * 5); i++) {
        $('.card2').append(`
            <div class="card mr-1" style="width: 13rem;" onclick="ShowDetailActor(${credits.cast[i].id})">
                <img src="${url_image}${credits.cast[i].profile_path}" class="card-img-top" alt="..." onerror="this.src='http://www.amulyamica.com/files/noimage.jpg'">
                <div class="card-body">
                <p class="card-text text-danger font-weight-bold">${credits.cast[i].character}</p>
                <p class="card-text">Name: ${credits.cast[i].name}</p>
                <p class="card-text">Sex: ${credits.cast[i].gender == 1 ? 'Female' : 'Male'}</p>
                </div>
            </div>
        `);
    }

    leng = Math.ceil(credits.cast.length / 5);
    console.log(leng);
    from = page - 1 == 0 ? 1 : page - 1;
    to = from + 4;

    if (to > leng) {
        to = leng;
        from = to - 5 < 1 ? 1 : to - 5;
    }

    $('.pagination').empty();
    $('.pagination').append(`
        <li class="page-item">
          <a class="page-link " href="#" tabindex="-1" aria-disabled="true" onclick="actorpagination(${id},1);">&laquo;</a>
        </li>
        
        
        <li class="page-item">
          <a class="page-link" href="#" onclick="actorpagination(${id},${leng});">&raquo;</a>
        </li>
        
    `)
    for (let i = from; i <= to; i++) {
        $('.pagination li:last-child').before(`<li class="page-item ${i === page ? 'active' : ''}"><a class="page-link" href="#" onclick="actorpagination(${id},${i});">${i}</a></li>`)
    }
    $('.recommendations').empty();
    if (listRecommendations && listRecommendations.results) {
        listRecommendations.results.length = 5;
        const renderHTMLRecommendations = listRecommendations.results.map(mv => `
    <a class="card mr-1" style="width: 13rem;" href="/movie/${mv.id}">
    <img src="${url_image}${mv.poster_path}" class="card-img-top" alt="..." onerror="this.src='http://www.amulyamica.com/files/noimage.jpg'">
    <div class="card-body">
    <p class="card-text text-danger font-weight-bold">${mv.original_title}</p>
    <p class="card-text">Release date: ${mv.release_date}</p>
    <p class="card-text">Popularity: ${mv.popularity}</p>
    </div>
  </a>
    `).join('\n');

        $('.pagination').after(`
    <div class="row1-md mt-2 recommendations">
      <h3 class="recommend-for-you">Recommend for you</h3>
      <div class="card3 d-flex flex-wrap justify-content-center mt-2">${renderHTMLRecommendations}</div>
    </div>
  `)
    }

}