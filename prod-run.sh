#!/bin/bash
cd ./movies-aws
ls
MY_IP=`curl ifconfig.me`
API_DETECT_ACTOR="http://${MY_IP}:5000"
echo $MY_IP
echo $API_DETECT_ACTOR
sudo kill $(sudo lsof -t -i:5000)
echo "Start python server";
sudo FLASK_APP=recognize-actor/router.py python3 -m flask run --host=0.0.0.0 --port=5000 &
echo "Start deploy container - [START]";
CONTAINER_NAMES=`sudo docker ps -aqf "name=movies-aws" --format "{{.Names}}"`
echo $CONTAINER_NAMES
if [ -n "${CONTAINER_NAMES}" ]
then
    DOCKER_STOP=`sudo docker stop movies-aws`
    DOCKER_RM=`sudo docker rm movies-aws`
fi
DOCKER_BUILD=`sudo docker build --tag movies-aws-image-1 .`
DOCKER_RUN=`sudo docker run -it -d --name movies-aws -e PORT=80 -e API_DETECT_ACTOR="${API_DETECT_ACTOR}" --restart=always -p 80:80 movies-aws-image-1`
DOCKER_PRUNE=`sudo docker image prune -a --force`
echo "Finish deploy container - [END]";
# pip3 install -r requirements.txt
# python3 recognize-actor/router.py

echo "Started success python server";
