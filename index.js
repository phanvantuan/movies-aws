const express = require('express');
const expHbs = require('express-handlebars')
const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs');
const https = require('https');
const options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};
require('dotenv').config()
const app = express();
app.engine('hbs', expHbs({
	// defaultLayout: 'main',
	extname: 'hbs',
	// layoutsDir: './views/layouts',
	// partialsDir: './views/partials',
	
}));
app.set('view engine', 'hbs');
app.set('views', './public');
app.use(express.static('public'));
app.use('/movie', express.static('public'));
app.use('*', express.static(__dirname + 'public'))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/movie/:id', async (req, res) => {
    return res.render('detail', {movieId: req.params.id})
})
const multer = require('multer');
const FormData = require('form-data');
const axios = require('axios');
app.post('/actors', async (req, res) => {
	let results= await handleUploadImage(req, res);
	return res.json(results.data || {code: 500})
})
app.get('/actors', async (req, res) => {
	return res.render('actors')
})
app.get('/', async (req, res) => {
    return res.render('movies')
})


const handleUploadImage = (req, res) => {
	return new Promise(async (resolve, rej) => {
		const upload = multer({
			storage: multer.memoryStorage()
		}).single('file');
		await upload(req, res, async (err) => {
			if (err) rej(err);
			const file = req.file;
			const buffer = file.buffer
			const formData = new FormData();
			formData.append('file', buffer, file.originalname);
			const config = {
				headers: {
					'content-type': 'multipart/form-data',
					...formData.getHeaders()
				}
			}
			let url = process.env.API_DETECT_ACTOR || `http://localhost:5000`
			console.log('Sending image to detect')
			let result = await axios.post(`${url}/file-upload`, formData, config).catch(e => {
				console.log('Failed call >>> ', e)
				return e;
			});
			resolve(result);
		})
		
	})
}

//* =============================================
//* Error handling
app.use((err, req, res, next) => {
	return res.status(500).json({
		 status: 500,
		 message: err.message,
	});
});

const PORT = process.env.PORT || 443;
// const server = https.createServer(options, app);
app.listen(PORT, () => console.log(`Server running on port ${PORT}!`));