FROM node:12.13-alpine
WORKDIR /usr/src/app
COPY package*.json ./
ENV PORT=80
RUN npm install
COPY . .
CMD [ "node", "index.js" ]
